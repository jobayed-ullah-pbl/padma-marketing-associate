<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Auth;

Route::get('clear-all', function () {
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    \Illuminate\Support\Facades\Artisan::call('clear-compiled');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    dd('Project Cache Clear');
});

Route::get('/', [HomeController::class, 'index']);
Route::Resource( 'application','App\Http\Controllers\ApplicationController');
Route::get('/application/{id}/forward', [App\Http\Controllers\ApplicationController::class, 'forward']);
Route::post('/application/{id}/forward', [App\Http\Controllers\ApplicationController::class, 'forwardStore']);
Route::get('/application/{id}/{action}/forward', [App\Http\Controllers\ApplicationController::class, 'forward']);
Route::post('/application/{id}/{action}/forward', [App\Http\Controllers\ApplicationController::class, 'forwardStore']);

Route::get('/application/{id}/associate-rm', [App\Http\Controllers\ApplicationController::class, 'associateRmCreate']);
Route::post('/application/{id}/associate-rm', [App\Http\Controllers\ApplicationController::class, 'associateRmStore']);


//Route::Resource( 'registration','App\Http\Controllers\UserController');
Route::get('/user-list', [App\Http\Controllers\UserController::class, 'index'])->name('user-list');
Route::get('/registration/{id}/edit', [App\Http\Controllers\UserController::class, 'edit']);
Route::patch('/user/{id}/', [App\Http\Controllers\UserController::class, 'update']);

Route::Resource( 'role','App\Http\Controllers\RoleController');



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
