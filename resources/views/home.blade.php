@extends('layouts.app')

@section('css')

@stop

@section('header')
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0">{{ $pageInfo['title'] }}</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <!--li class="breadcrumb-item active"></li-->
        </ol>
    </div>
</div>
@stop

@section('content')
        @auth
        <div class="col-lg-12">
            <!-- CARD -->
            <div class="card">
                <!-- CARD HEADER-->
                <div class="card-header border-0">
                    <h3 class="card-title">Application List</h3>
                    <div class="card-tools">
                        <a href="#" class="btn btn-tool btn-sm">
                            <i class="fas fa-download"></i>
                        </a>
                        <a href="#" class="btn btn-tool btn-sm">
                            <i class="fas fa-bars"></i>
                        </a>
                    </div>
                </div>
                <!-- CARD HEADER ENDS-->

                <!-- CARD BODY-->
                <div class="card-body table-responsive">
                    @if (isset($Applications) )
                    <table id="table_list" class="table table-bordered table-striped table-valign-middle">
                        <thead>
                            <tr>
                                <th>Applicant</th>
                                <th>Date of Birth</th>
                                <th>Pref. Branch</th>
                                <th>Status</th>
                                <th class="text-right py-0 align-middle">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                    @foreach ($Applications as $Application)

                        <tr>
                            <td>
                                @if ($Application->applicant_image!='placeholder.png')
                                <img src="{{ url('images/applicants/'.$Application->applicant_image) }}" alt="Applicant 1" class="img-circle img-sm"> &nbsp;
                                @endif
                                
                                {{$Application->name}}
                            </td>
                            <td>{{ $Application->getDob($Application->dob) }}</td>
                            <td>{{ $Application->getPreferredBranchName->BR_NAME }}</td>
                            <td>
                            @if (isset($Application->status))
                                
                                @if ($Application->status==101)
                                    <span class="right badge badge-success">Accepted</span>
                                @elseif ($Application->status==102)
                                    <span class="right badge badge-danger">Declined</span>
                                @else
                                    <span class="right badge badge-primary">New</span>
                                @endif
                            @endif
                            </td>
                            
                            <td class="text-right py-0 align-middle">
                                <div class="btn-group btn-group-sm">
                                  <a href="{{ url('application/'.$Application->id) }}" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="view"><i class="fas fa-eye"></i></a>
                                  @auth
                                    @if (auth()->user()->getRoleName->role_short_name=='br')
                                        @if ($Application->status==101)
                                            <a href="{{ url('application/'.$Application->id.'/associate-rm') }}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Assign RM"><i class="fas fa-user-check"></i></a>
                                        @endif
                                    @endif
                                  @endauth
                                  
                                  @auth
                                    @if (auth()->user()->getRoleName->role_short_name!=='ho' && $Application->status!=101)
                                        <a href="{{ url('application/'.$Application->id.'/forward') }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="forward"><i class="fas fa-directions"></i></a>
                                    @endif
                                  @endauth
                                  
                                  @auth
                                    @if (auth()->user()->getRoleName->role_short_name==='ho')
                                        <a href="{{ url('application/'.$Application->id.'/AC'.'/forward') }}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Accept"><i class="fas fa-check"></i></a>
                                        <a href="{{ url('application/'.$Application->id.'/DEC'.'/forward') }}" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Decline"><i class="fas fa-times"></i></a>
                                    @endif
                                  @endauth
                                  
                                </div>
                            </td>
                        </tr>
                        
                    @endforeach
                        </tbody>
                        
                    </table>
                        
                    @endif
                    
                        
                    
                        
                </div>
                <!-- END CARD BODY -->
            </div>
            <!-- END CARD -->
        </div>
        @endauth

        

        @guest
        <div class="callout callout-info">
            <h1 class="font-weight-light">Welcome to Padma Bank Marketing Associate.</h1>
            
        </div>
        <div class="card-body row">
            <a href="{{ url('application') }}" type="button" class="btn btn-info btn-block btn-flat">APPLY NOW <i class="fa fa-location-arrow"></i></a>
        </div>
        
        @endguest
@stop

@section('scripts')

<script>

$(function(){
    $("#table_list").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#table_list_wrapper .col-md-6:eq(0)');
});
</script>

@stop