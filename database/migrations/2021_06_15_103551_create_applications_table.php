<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id()->nullable();

            $table->string('name',100)->nullable();
            $table->date('dob')->nullable();
            $table->string('place_of_birth',100)->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->tinyInteger('marital_status')->nullable();
            $table->string('nid_no',20)->nullable();
            
            $table->string('pre_road_or_village',100)->nullable();
            $table->string('pre_post_office',100)->nullable();
            $table->tinyInteger('pre_division')->nullable();
            $table->tinyInteger('pre_district')->nullable();
            $table->tinyInteger('pre_years')->nullable();

            $table->string('per_road_or_village',100)->nullable();
            $table->string('per_post_office',100)->nullable();
            $table->tinyInteger('per_division')->nullable();
            $table->tinyInteger('per_district')->nullable();
            
            /** contact info */
            $table->string('cell',11)->nullable();
            $table->tinyInteger('cell_years')->nullable();
            $table->string('email',100)->nullable();
            $table->string('tin',20)->nullable();
            
            $table->string('social',100)->nullable();
            $table->tinyInteger('social_years')->nullable();
            $table->string('occupation',100)->nullable();
            $table->string('bank_acc_no',13)->nullable();
            $table->string('bank_name',50)->nullable();
            $table->string('branch',100)->nullable();
            
            // Education
            $table->string('edu_institute',100)->nullable();
            $table->string('name_of_exam',50)->nullable();
            $table->tinyInteger('year_of_passing')->nullable();
            $table->string('cgpa_division_class',50)->nullable();
            $table->string('name_board_university',100)->nullable();
            $table->string('professional_degree',100)->nullable();

            //emergency contact details
            $table->string('primary_contact_name',100)->nullable();
            $table->string('primary_contact_relationship',100)->nullable();
            $table->string('primary_contact_phn1',11)->nullable();
            $table->string('primary_contact_phn2',11)->nullable();

            $table->string('secondary_contact_name',100)->nullable();
            $table->string('secondary_contact_relationship',100)->nullable();
            $table->string('secondary_contact_phn1',11)->nullable();
            $table->string('secondary_contact_phn2',11)->nullable();

            $table->string('employee_reference_name',100)->nullable();
            $table->string('employee_reference_relationship',100)->nullable();
            $table->string('employee_reference_employee_id',20)->nullable();
            $table->string('employee_reference_contact',11)->nullable();

            // Family info
            $table->string('spouse_name',100)->nullable();
            $table->date('spouse_dob')->nullable();
            $table->string('spouse_contact',11)->nullable();

            $table->string('fathers_name',100)->nullable();
            $table->date('fathers_dob')->nullable();
            $table->string('fathers_contact',11)->nullable();

            $table->string('mothers_name',100)->nullable();
            $table->date('mothers_dob')->nullable();
            $table->string('mothers_contact',11)->nullable();

            // Provide Info
            $table->string('bankruptcy',3)->nullable();
            $table->string('bankruptcy_details',150)->nullable();

            $table->string('borrower',3)->nullable();
            $table->string('borrower_details',150)->nullable();

            $table->string('convicted_by_court',3)->nullable();
            $table->string('convicted_by_court_details',150)->nullable();

            $table->string('trade_union',3)->nullable();
            $table->string('trade_union_details',150)->nullable();

            $table->string('member',3)->nullable();
            $table->string('member_details',150)->nullable();

            //image
            $table->string('applicant_image',150)->nullable();
            $table->tinyInteger('status')->default(1);






            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
