<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

use App\Models\Application;
use App\Models\Branch;
use App\Models\Forward_log;
use App\Models\Role;
use App\Models\User;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class ApplicationController extends Controller
{
    public function index()
    {

        $genderList = ['Male','Female','Others'];
        $statusList = ['Inactive','Active'];
        $maritalStatusList = ['Single','Married','Others'];
        $branchList = $this->makeDD(DB::table('branch')->pluck('BR_NAME','T24_BR'));
        $divisionList = $this->makeDD(DB::table('divisions')->pluck('NAME','ID'));
        $districtList = $this->makeDD(DB::table('districts')->pluck('NAME','ID'));

        $pageInfo = [
            "title"=>"Application",
            "route"=>"application"
        ];

        return view('application.create',compact('genderList','statusList','maritalStatusList',
        'branchList','divisionList','districtList','pageInfo') );
    }

    public function store(Request $request){
        $inputs = $request->all();
        //dd($inputs);

        $niceNames = [
            'pre_road_or_village' => 'Present Road or Village Adreess',
            'pre_post_office' => 'Present Post Office',
            'pre_years' => 'Presently Staying ',
            'per_road_or_village' => 'Permanent Road or Village Adreess',
            'per_post_office' => 'Permanent Post Office',
            'cell_years' => 'Cell Using(in years)',
            'social' => 'FB / LinkedIn / Instagram ID',
            'social_years' => 'How long been using this ID?',
            'edu_institute' => 'education institute',
            'cgpa_division_class' => 'cgpa / division / class',
            'name_board_university' => 'Name of Board / University',

            'spouse_dob' => 'spouse date of birth',
            'fathers_dob' => 'fathers date of birth',
            'mothers_dob' => 'mothers date of birth',

            'bankruptcy_details'=>'Specify Details',
            'borrower_details'=>'Specify Details',
            'convicted_by_court_details'=>'Specify Details',
            'trade_union_details'=>'Specify Details',
            'member_details'=>'Specify Details',



        ];
    	$validator = Validator::make($inputs, array(
            'name'          => 'required|min:1|max:100|alpha_spaces',
            'dob'           => 'required|date',
            'passport_no'   => 'alpha_num|min:10|max:15|required_without:nid_no|nullable',
            'nid_no'        => 'digits_between:10,17|required_without:passport_no|nullable',
            'place_of_birth'=>'required|alpha',
            'applicant_image'=>'required|file|mimes:jpeg,png,jpg|max:2048',

            'pre_road_or_village'=>'required',
            'pre_post_office'=>'required',
            'pre_years'=>'required|int',

            'per_road_or_village'=>'required',
            'per_post_office'=>'required',

            'cell' => 'required|digits:11|regex:/(01)[0-9]{9}/',
            'cell_years'=>'required|int',
            'email'=>'required|email',
            'tin'=>'nullable|digits:9',
            'social'=>'required|max:100',
            'social_years'=>'required|digits_between:1,2',
            'occupation'=>'nullable|alpha_spaces|max:100',
            'bank_acc_no'=>'nullable|digits:13',
            'bank_name'=>'nullable|max:50',
            'branch'=>'nullable|max:100',

            'edu_institute'=>'nullable|max:100',
            'name_of_exam'=>'nullable|max:50',
            'year_of_passing'=>'nullable|digits:4',
            'cgpa_division_class'=>'nullable|max:6',
            'name_board_university'=>'nullable|max:100',
            'professional_degree'=>'nullable|max:100',

            'spouse_name'=>'required|max:100',
            'spouse_dob'=>'nullable|date',
            'spouse_contact'=>'nullable|digits:11|regex:/(01)[0-9]{9}/',
            'fathers_name'=>'required|max:100',
            'fathers_dob'=>'nullable|date',
            'fathers_contact'=>'nullable|digits:11|regex:/(01)[0-9]{9}/',
            'mothers_name'=>'required|max:100',
            'mothers_dob'=>'nullable|date',
            'mothers_contact'=>'nullable|digits:11|regex:/(01)[0-9]{9}/',

            'primary_contact_name'=>'required|max:100',
            'primary_contact_relationship'=>'required|max:100',
            'primary_contact_phn1'=>'required|digits:11|regex:/(01)[0-9]{9}/',
            'primary_contact_phn2'=>'nullable|digits:11|regex:/(01)[0-9]{9}/',
            'secondary_contact_name'=>'nullable|max:100',
            'secondary_contact_relationship'=>'nullable|max:100',
            'secondary_contact_phn1'=>'nullable|digits:11|regex:/(01)[0-9]{9}/',
            'secondary_contact_phn2'=>'nullable|digits:11|regex:/(01)[0-9]{9}/',
            'employee_reference_name'=>'nullable|max:100',
            'employee_reference_relationship'=>'nullable|max:100',
            'employee_reference_employee_id'=>'nullable|max:9',
            'employee_reference_contact'=>'nullable|digits:11|regex:/(01)[0-9]{9}/',

            'bankruptcy_details'=>'nullable|max:150',
            'borrower_details'=>'nullable|max:150',
            'convicted_by_court_details'=>'nullable|max:150',
            'trade_union_details'=>'nullable|max:150',
            'member_details'=>'nullable|max:150',


    	),
        array(
            'passport_no.required_without' => 'The Passport field is required when NID field not present.',
            'nid_no.required_without' => 'The NID field is required when Passport field not present.',
            'name.alpha_spaces' =>'Name will contain only alphabets and characters.',
            'occupation.alpha_spaces' =>'Occupation will contain only alphabets and characters.'
        ),
        $niceNames);
        
    	if ($validator -> fails()) {
            
            return Redirect() -> back() -> withErrors($validator) -> withInput();
    	}

        // Image upload
        if ($request->hasFile('applicant_image')) 
        {
            $imageName = time().'.'.request()->applicant_image->getClientOriginalExtension();
            request()->applicant_image->move(public_path('images/applicants/'), $imageName);
        } else {
            $imageName = 'placeholder.png';
        }

        $inputs['applicant_image'] = $imageName;

        if($inputs['dob']!=NULL)
            $inputs['dob'] = Carbon::createFromFormat('m/d/Y', $request->dob)->format('Y-m-d');

        if($inputs['spouse_dob']!=NULL)
            $inputs['spouse_dob'] = Carbon::createFromFormat('m/d/Y', $request->spouse_dob)->format('Y-m-d');
        if($inputs['fathers_dob']!=NULL)
            $inputs['fathers_dob'] = Carbon::createFromFormat('m/d/Y', $request->fathers_dob)->format('Y-m-d');
        if($inputs['mothers_dob']!=NULL)
            $inputs['mothers_dob'] = Carbon::createFromFormat('m/d/Y', $request->mothers_dob)->format('Y-m-d');

           
    	$br = new Application();
        $br->fill($inputs)->save();

        session()->flash('message_type', 'success');
        session()->flash('message', 'New Application Successfully Saved');
    	return Redirect() -> to('/'); 
    }

    public function show($id){
        $pageInfo = [
            "title"=>"Application View",
            "route"=>"application"
        ];
        $Application = Application::where('id', $id)->first();
        $Remarks = Forward_log::where('application_id', $id)->get();
        return view('application.view',compact('pageInfo','Application','Remarks'));
    }

    public function forward($id,$action=NULL){
        $pageInfo = [
            "title"=>"Forward Application",
            "route"=>"application",
            "action"=>((isset($action))? $action : NULL)
        ];

        $Application = Application::where('id', $id)->first();
        $Remarks = Forward_log::where('application_id', $id)->get();
        return view('application.forward',compact('pageInfo','Application','Remarks'));
    }

    public function forwardStore(Request $request,$id,$action=NULL){
        $inputs = $request->all();
        $user = Auth::user();

        $validator = Validator::make($inputs, array(
    		'remarks' => [ 'nullable','string', 'max:499']
    	));
        
    	if ($validator -> fails()) {
            return Redirect() -> back() -> withErrors($validator) -> withInput();
    	}

        
        $forward_from = 1;
        $forward_to = 1;
        switch ($user->getRoleName->role_short_name) 
        {
            case 'br':
                $forward_from = $user->branch_id;
                $forward_to = Branch::getCluster($user->branch_id);
                break;
            
            case 'cm':
                $forward_from = Branch::getCluster($user->branch_id);
                $forward_to = 100;
                break;
            case 'ho':
                $forward_from = 'HO';
                $forward_to = 'BR';
                break;
            default:
                break;
        }

        $status = $user->getRoleName->parent_role_id;
        if($action != NULL){
            if($action=='AC')
            {
                $status = 101;
            }
            else if($action === 'DEC') $status = 102;
        }
        
        $Application = Application::where('id',$id)->first();
        $Application->status     =  $status; //ACCEPT - 101// DECLINE - 102
        $Application->updated_by = $user->id;
        $statusChanged = $Application->save();

        if($action != NULL && $action=="AC" && $statusChanged){
            User::create([
                'name' => $Application->name,
                'employee_id' => " ",
                'role_id' => Role::firstWhere('role_short_name', 'assc')->id,
                'branch_id' => $Application->preferred_branch,
                'email' => $Application->email,
                'username' => $Application->email,
                'image' => $Application->applicant_image,
                'password' => Hash::make('12345678'),
            ]);

            $from_path = public_path('images/applicants/'.$Application->applicant_image);
            if(file_exists($from_path) ) {
                File::copy($from_path,
                    public_path('images/user/'.$Application->applicant_image));
            }

            
        }

        $ret = Forward_log::create([
            'application_id' => $id,
            'forward_from' => $forward_from,
            'forward_to' => $forward_to,
            'remarks' => $request->remarks
        ]);

        return redirect()->to('/');
    }

    public function associateRmCreate($id)
    {
        $pageInfo = [
            "pageTitle"=>"Assign RM",
            "route"=>"application"
        ];
        $Application = Application::where('id', $id)->first();
        $rmList = $this->makeDD(User::where('branch_id', Auth::user()->branch_id)
                        ->where('role_id',Role::firstwhere('role_short_name','rm')->id)
                        ->get()->pluck('name','id'));
        return view('rm.create',compact('pageInfo','rmList','Application'));
    }

    public function associateRmStore(Request $request,  $id)
    {
        //dd("Error");
        $inputs = $request->all();
        $Application = Application::where('id', $id)->first();

        $validator = Validator::make($inputs, array(
    		'rm_id' => [ 'required','int']
    	));
    	if ($validator -> fails()) {
            dd("error");
            return Redirect() -> back() -> withErrors($validator) -> withInput();
    	}

        $Application->rm_id = $request->rm_id;
        $Application->save();

        return redirect()->to('/')->withSuccess("RM Assigned for Marketing Associates successfully");

    }

    
}
