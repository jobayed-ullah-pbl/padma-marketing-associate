<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Application;
use App\Models\Branch;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $roleName = $user->getRoleName->role_short_name;
            
            switch ($roleName) 
            {
                case 'admin':
                    $Applications = Application::all();
                    break;

                case 'br':
                    $Applications = Application::where(function($query)
                                                {
                                                    $query->where('status',1);
                                                    $query->orwhere('status',101);
                                                    $query->orwhere('status',102);
                                                })
                                                ->where('preferred_branch',$user->branch_id)
                                                ->wherenull('rm_id')
                                                ->get();
                    break;
                case 'cm':
                    $cluster_branches = Branch::where('CLUSTER_ID', Branch::getCluster( $user->branch_id) )
                        ->get();
                    $Applications = Application::where('status',$user->getRoleName->id)
                                               ->whereIn('preferred_branch',$cluster_branches->pluck('T24_BR'))
                                                ->get();
                    break;

                case 'ho':
                    $Applications = Application::where('status',$user->getRoleName->id)
                                                ->get();
                    break;
                case 'rm':
                    $Applications = Application::where('status',101)
                                                ->where('rm_id',$user->id)
                                                ->where('preferred_branch',$user->branch_id)
                                                ->get();
                    break;
                
                
                default:
                    $Applications = Application::where('status',$user->getRoleName->role_id)
                                                ->get();
                    break;
            }
            
        }
        else $Applications = [];
        

        $pageInfo = [
            "title"=>"Home",
            "route"=>"home"
        ];
        return view('home',compact('Applications','pageInfo'));
    }
}
