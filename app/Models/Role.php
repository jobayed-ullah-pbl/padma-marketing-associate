<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "roles";
    protected $guarded = ['id'];

    public static function getRoleShortName($value)
    {
        return Role::firstWhere('id', $value)->role_short_name;
    }
}
