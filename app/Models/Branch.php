<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;
    protected $table = 'branch';

    public static function getCluster($value)
    {
        return Branch::firstWhere('T24_BR', $value)->CLUSTER_ID;
    }
}
