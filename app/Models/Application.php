<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Districts;
use App\Models\Divisions;
use App\Models\Branch;

class Application extends Model
{
    use HasFactory;
    protected $table = 'applications';
    protected $guarded = ['id'];

    
    public function getDob($value)
    {
        return Carbon::parse($value)->format('d M, Y');
    }

    public function getDistrictPre()
    {
        return $this->belongsTo(Districts::class, 'pre_district', 'ID');
    }

    public function getDivisionPre()
    {
        return $this->belongsTo(Divisions::class, 'pre_division', 'ID');
    }

    public function getDistrictPer()
    {
        return $this->belongsTo(Districts::class, 'per_district', 'ID');
    }

    public function getDivisionPer()
    {
        return $this->belongsTo(Divisions::class, 'per_division', 'ID');
    }

    public function getBranchName()
    {
        return $this->belongsTo(Branch::class, 'branch', 'T24_BR');
    }
    public function getPreferredBranchName()
    {
        return $this->belongsTo(Branch::class, 'preferred_branch', 'T24_BR');
    }

}
